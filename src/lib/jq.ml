open! Base
include Ezjsonm

type t =
  [ `A of t list
  | `Bool of bool
  | `Float of float
  | `Null
  | `O of (string * t) list
  | `String of string ]
[@@deriving show]

let get_field f assoc : t =
  try List.Assoc.find_exn assoc f ~equal:String.equal
  with _ ->
    Fmt.kstr failwith "Jq.get_field: %S not found in %s" f
      (value_to_string (`O assoc))

let ( @@@ ) : t -> t -> t =
 fun a b ->
  match (a, b) with
  | `O la, `O lb -> `O (la @ lb)
  | `A la, `A lb -> `A (la @ lb)
  | _ ->
      Fmt.failwith
        "Json-construction: invalid types (need both to be arrays or objects)"

let ( --> ) x b = dict [(x, b)]
