open! Base

module Mr_warnings = struct
  type t = string * int * string list option

  let compare ((prj_id1, mr_id1, _) : t) ((prj_id2, mr_id2, _) : t) : int =
    match String.compare prj_id1 prj_id2 with
    | 0 -> Int.compare mr_id1 mr_id2
    | c -> c

  let concat ((prj_id1, mr_id1, s1) : t) ((_, _, s2) : t) : t =
    match (s1, s2) with
    | None, None -> (prj_id1, mr_id1, None)
    | Some s, None -> (prj_id1, mr_id1, Some s)
    | None, Some s -> (prj_id1, mr_id1, Some s)
    | Some s1, Some s2 -> (prj_id1, mr_id1, Some (s1 @ s2))
end

type t = {pipeline_link: string; mutable project_warnings: Mr_warnings.t list}

let make (pipeline_link : string) : t = {pipeline_link; project_warnings= []}

let proj_warnings_add project_warnings mr_warning : Mr_warnings.t list =
  let open Mr_warnings in
  let rec aux = function
    | [] -> [mr_warning]
    | h :: t -> (
      match compare h mr_warning with
      | 0 -> concat h mr_warning :: t
      | _ -> h :: aux t ) in
  aux project_warnings

(** [add warnings mr_id s] is [warnings] with a new message [s] for merge
    request [mr]. *)
let add ~(warnings : t) ?(s : string option) ~(mr : Merge_request.t) () : unit =
  let mr_warnings =
    (function
      | None -> (mr.project.path_with_namespace, mr.id, None)
      | Some s -> (mr.project.path_with_namespace, mr.id, Some [s]) )
      s in
  let p = proj_warnings_add warnings.project_warnings mr_warnings in
  warnings.project_warnings <- p

(** [warnings_to_string warnings] is a string of [warnings] formated to post in
    a Gitlab issue thread *)
let warnings_to_string (warn_on : [`Always | `Non_empty]) (warnings : t) :
    string =
  let open Fmt in
  let prj_warnings =
    match warn_on with
    | `Always -> warnings.project_warnings
    | `Non_empty ->
        List.filter warnings.project_warnings ~f:(fun (_, _, s) ->
            Option.is_some s ) in
  let s =
    List.concat_map prj_warnings ~f:(fun (prj_id, mr_id, sl) ->
        str "* Warnings for project: %S,  merge request: %d" prj_id mr_id
        ::
        List.map ~f:(str "  * %s")
          ((function None -> ["No warnings!"] | Some w -> w) sl) )
    |> String.concat ~sep:"\n" in
  str "This is a Merbocop generaged warning for Pipeline: %S\n%s"
    warnings.pipeline_link s

(** [post_issue_comment token project issue_id retry_options warnings] posts a
    comment (discusion thread) under [issue_id] in [project]. [warn_on] `Always
    will post comments for merge requests which have no warnings. *)
let post_issue_comment ?(token : string option) ~(project : int)
    ~(issue_id : int) ~(warn_on : [`Always | `Non_empty])
    ~(retry_options : Web_api.Retry_options.t) (warnings : t) =
  let has_warnings =
    List.exists warnings.project_warnings ~f:(fun (_, _, s) -> Option.is_some s)
  in
  try
    match (warn_on, has_warnings) with
    | `Non_empty, false -> Ok None
    | _, _ ->
        Ok
          (Some
             ( warnings_to_string warn_on warnings
             |> Gitlab.post_issue_thread ?token ~project ~issue_id
                  ~retry_options ) )
  with e -> Error e
